#!/usr/bin/python
"""\
Maintainer: Claudiu
"""

import logging
import time
import requests
import urllib3

urllib3.disable_warnings()

logging.basicConfig(filename='logging.log', level=logging.INFO,
		    format='%(asctime)s:%(message)s',
		    datefmt='%m/%d/%Y %I:%M:%S %p',
		    filemode='a')

def main():
    res = requests.get('https://www.wikipedia.com', verify=False)
    response = requests.post(res.url)
    logging.info(res.url)
    logging.info(res.status_code)
    logging.info(response.elapsed.total_seconds())
    print(res.status_code)

N = 10
for i in range(0, N):
    main()
    time.sleep(1)
